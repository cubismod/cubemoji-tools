#!/bin/ash
set -e 
set -o pipefail

FLY_EXEC=$HOME/.fly/bin/flyctl

# get flyctl
curl -L https://fly.io/install.sh | sh

${FLY_EXEC} deploy . --remote-only

# log DIFF
DIFF=$(git diff HEAD^ HEAD fly.toml)

echo "$DIFF"
