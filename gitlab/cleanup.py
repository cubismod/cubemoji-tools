# script designed to clean up gitlab MRs that have been stale
import datetime
import logging
import os

import gitlab
from dotenv import load_dotenv
from gitlab.v4.objects import MergeRequestManager


def check_job_token():
    if "CI_JOB_TOKEN" in os.environ:
        return os.environ["CI_JOB_TOKEN"]
    return ""


def get_mrs(gl: gitlab.client.Gitlab):
    if "DESIRED_PROJECT_ID" in os.environ:
        project = gl.projects.get(os.environ["DESIRED_PROJECT_ID"])
        return project.mergerequests


def clean_mrs(mrs: MergeRequestManager):
    date = datetime.datetime.now()

    month_ago = date - datetime.timedelta(days=60)
    mr_list = mrs.list(get_all=True, updated_before=month_ago, state="opened")

    for mr in mr_list:
        logging.info(f'Closing merge request #{mr.id}, "#{mr.title}" at {mr.web_url}')
        mr.labels.append("stale")
        mr.state_event = "close"
        mr.save()


def main():
    load_dotenv()
    logging.basicConfig(level=logging.INFO)

    gl = gitlab.Gitlab()
    if "GITLAB_API_TOKEN" in os.environ:
        gl = gitlab.Gitlab(private_token=os.environ["GITLAB_API_TOKEN"])
    else:
        job_token = check_job_token()
        if job_token != "":
            gl = gitlab.Gitlab(job_token=job_token)

    mrs = get_mrs(gl)
    clean_mrs(mrs)


if __name__ == "__main__":
    main()
