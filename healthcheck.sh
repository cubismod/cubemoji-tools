#!/bin/ash
http_code=$(curl -LI http://cubemoji.art/status -o /dev/null -w '%{http_code}\n' -s)

if [ "${http_code}" -eq 200 ]; then
  echo "healthcheck passed"
else
  FLY_EXEC=$HOME/.fly/bin/flyctl

  # get flyctl
  curl -L https://fly.io/install.sh | sh

  ${FLY_EXEC} apps restart cubemoji
fi
