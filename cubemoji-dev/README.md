# Cubemoji Dev
A development Fly environment for cubemoji.  
This runs using the NPR token used for testing but connected with the entire
logging and tracing as well as S3 access and a frontend web server to validate
that the deployment process works.

## Testing Process
```
fly scale count 1
fly deploy . --remote-only
```
