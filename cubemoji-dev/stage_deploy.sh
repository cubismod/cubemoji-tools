#!/bin/ash
set -e 
set -o pipefail

# Deploys cubemoji to testing environment for 2 minutes
# This script should be run in the current directory as it relies on ./fly.toml

# in order to prevent a race condition of multiple staging instances trying to run at once,
# we check the staging status url to ensure that nothing is running or exit
if curl https://cubemoji-dev.fly.dev/status; then
  echo "Staging instance already running! Waiting 1 min."
  sleep 60
fi

FLY_EXEC=$HOME/.fly/bin/flyctl

# get flyctl
curl -L https://fly.io/install.sh | sh

${FLY_EXEC} scale count 1 -y

${FLY_EXEC} deploy . --remote-only --detach

${FLY_EXEC} logs &

echo "sleeping for 1 min"
sleep 60

kill $!

${FLY_EXEC} scale count 0 -y
