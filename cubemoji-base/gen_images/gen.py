#!/bin/python3
import shutil
import sys
from concurrent import futures
from concurrent.futures import ThreadPoolExecutor
from logging import INFO, basicConfig, error, info
from os import getenv, mkdir
from pathlib import Path, PurePath
from shutil import copyfile
from tempfile import TemporaryDirectory
from urllib import request
from zipfile import ZipFile

urls = [
    "https://s3.us-east-1.wasabisys.com/cubemoji/mtnt_2020.04_short_png128.zip",
    "https://s3.us-east-1.wasabisys.com/cubemoji/mtnt_special_s3.zip",
    "https://s3.us-east-1.wasabisys.com/cubemoji/mutstd_vip_2018.04_all.zip",
    "https://s3.us-east-1.wasabisys.com/cubemoji/assets.zip",
]


def download(source: str, emoji_dest: Path, favicon_dest: Path):
    with TemporaryDirectory() as tmp_dir:
        dest = emoji_dest

        file = Path(tmp_dir, "dl.zip")
        request.urlretrieve(source, file)
        with ZipFile(file) as zip:
            zip.extractall(path=tmp_dir)

        parse_dir = Path(tmp_dir)
        if source == urls[0]:
            parse_dir = parse_dir.joinpath("mtnt_2020.04_short_png128", "emoji")
        elif source == urls[1]:
            parse_dir = parse_dir.joinpath("mtnt_special_s3", "emoji", "png-128")
        elif source == urls[2]:
            parse_dir = parse_dir.joinpath(
                "mutstd_vip_2018.04_all", "emoji", "png-128px"
            )
        elif source == urls[3]:
            # parse favicons here
            parse_dir = parse_dir.joinpath("assets", "favicon")
            dest = favicon_dest

        glob_patterns = ["**/*.png", "**/*.ico", "**/*.webmanifest"]
        for pattern in glob_patterns:
            for img_path in parse_dir.glob(pattern):
                copyfile(img_path, Path(dest, img_path.name))


if __name__ == "__main__":
    basicConfig(level=INFO)
    info("Starting image extraction process!")

    # here we will extract various emoji packs to disk
    # to serve up statically with Fly.io

    base_path = Path("/usr", "src", "cubemoji", "static/")
    emoji_dest = base_path / "emoji"
    emoji_dest.mkdir(exist_ok=True, parents=True)

    favicon_dest = base_path / "favicon"
    favicon_dest.mkdir(exist_ok=True, parents=True)

    with ThreadPoolExecutor() as exec:
        future_list = {
            exec.submit(download, url, emoji_dest, favicon_dest): url for url in urls
        }
        for future in futures.as_completed(future_list):
            try:
                future.result()
            except Exception as exc:
                error(exc)
    info("Downloaded and extracted images!")
