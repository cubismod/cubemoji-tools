#!/bin/ash
set -e 
set -o pipefail

# build imagemagick so that we can have proper rescale support
# default alpine pkg lacks this
apk upgrade
apk add --no-cache build-base g++ glib glib-dev wget git pkgconfig libjpeg-turbo-dev libpng-dev xz-libs zlib libx11-dev

# liquid rescale setup
mkdir -p /usr/src/lr
cd /usr/src/lr
wget https://s3.us-east-1.wasabisys.com/cubemoji/liblqr-1-0.4.2.tar.bz2
tar -vxjf liblqr-1-0.4.2.tar.bz2
cd liblqr-1-0.4.2
./configure
make -j 3
make install -j 3
rm -rf /usr/src/lr


# imagemagick setup
mkdir -p /usr/src/im
cd /usr/src/im
git clone --depth 1 https://github.com/ImageMagick/ImageMagick.git ImageMagick-7.1.0 
cd ImageMagick-7.1.0
./configure --without-perl --without-wmf --without-tiff --with-quantum-depth=8
make -j 3
make install -j 3
rm -rf /usr/src/im
