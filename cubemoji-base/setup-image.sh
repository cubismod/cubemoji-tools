#!/bin/ash
set -e 
set -o pipefail

# update pkgs
# https://github.com/node-gfx/node-canvas-prebuilt/issues/77
apk update
apk upgrade
apk add --no-cache graphicsmagick python3 pixman cairo-dev jpeg-dev pango-dev giflib-dev git libwebp libwebp-dev curl

cd /usr/src/cubemoji
mkdir download
mkdir -p data/backups
mkdir data/logs
mkdir -p static/list
mkdir -p static/emotes
